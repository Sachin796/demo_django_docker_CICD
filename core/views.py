from django.shortcuts import render
import requests as req
import environ

env = environ.Env()
# reading .env file
environ.Env.read_env()
# from dotenv import dotenv_values
# config = dotenv_values(".env")


def home(request):
    print(env("URL"))
    return render(request, 'home.html')


def fetchedData(request):
    print('Fetched data')
    print(dir(env))
    url = env('URL_LINk')
    data = req.get(url).json()
    return render(request, 'fetchedData.html', {'data': data})
