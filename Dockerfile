# Download python specified version image from Docker Hub
FROM python:3.9-alpine

ENV PYTHONUNBUFFERED 1

RUN apk update && apk add --no-cache python3-dev && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir --upgrade setuptools && \
    pip3 install --no-cache-dir --upgrade wheel && \
    pip3 install --no-cache-dir --upgrade virtualenv && \
    pip3
# Create a work directory named demoapp
WORKDIR /demoapp

# Copy all contens of the current directory to the demoapp directory
COPY . /demoapp

# Install requirements
RUN pip3 install -r requirements.txt

# RUN the application inside the container at port 8000
CMD [ "python3" , "manage.py" , "runserver", "0.0.0.0:8000" ]
